<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="LibCereal-1.0" version="1.0.6-release">

		<Author name="nemes" />

		<Description text="Library to serialise LUA objects as wstrings" />

		<VersionSettings gameVersion="1.2.1" windowsVersion="1.0" savedVariablesVersion="1.0" />

		<Dependencies/>

		<Files>
			<File name="source/lib/LibStub.lua" />
			<File name="source/LibCereal.lua" />
		</Files>

		<OnInitialize/>

		<OnUpdate/>

		<OnShutdown/>

		<SavedVariables/>

	</UiMod>
</ModuleFile>
