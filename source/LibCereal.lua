-- library creation
local MAJOR, MINOR = "LibCereal-1.0", 6
local LibCereal, oldminor = LibStub:NewLibrary(MAJOR, MINOR)
if not LibCereal then return end

-- local function declarations
local escapeSpecial
local unEscapeSpecial
local serialise
local deserialise

-- local variables - not threadsafe!
local stringCompressionTable = false


--
-- PUBLIC FUNCTIONS
--

-- serialise an LUA object into a single wstring - any functions present will cause an error
function LibCereal.serialise(o, stringCompression)
	if stringCompression then
		stringCompressionTable = {}
		for i, str in ipairs(stringCompression) do
			stringCompressionTable[str] = "�"..tostring(i)
		end
	else
		stringCompressionTable = false
	end
	return serialise(o, 1)
end

-- deserialise a wstring output by LibCereal.serialise, returning the table or value
function LibCereal.deserialise(str, stringCompression)
	if stringCompression then
		stringCompressionTable = {}
		for i, str in ipairs(stringCompression) do
			stringCompressionTable["�"..tostring(i)] = str
		end
	else
		stringCompressionTable = false
	end
	return deserialise(str, 1)
end


--
-- PRIVATE FUNCTIONS
--

-- escape with a leading backslash: = " , { }
escapeSpecial = function(str)
	if not str or str:len() == 0 then
		return L""
	else
		return str:gsub(L"=", L"\\="):gsub(L",", L"\\,"):gsub(L"\"", L"\\\""):gsub(L"{", L"\\{"):gsub(L"}", L"\\}"):gsub(L"�", L"�\\")
	end
end

-- un-escape backslashed: = " , { } �
unEscapeSpecial = function(str)
	if not str or str:len() == 0 then
		return L""
	else
		return str:gsub(L"\\=", L"="):gsub(L"\\,", L","):gsub(L"\\\"", L"\""):gsub(L"\\{", L"{"):gsub(L"\\}", L"}"):gsub(L"�\\", L"�")
	end
end

-- serialise an LUA object into a single wstring - any functions present will cause an error
serialise = function(o, level)

	local result

	if type(o) == "number" then
		result = L"N"..towstring(o)
	elseif type(o) == "string" then
		local compressed = stringCompressionTable and stringCompressionTable[o] or false
		if compressed then
			result = L"S"..towstring(compressed)
		else
			result = L"S\""..escapeSpecial(towstring(o))..L"\""
		end
	elseif type(o) == "wstring" then 
		result = L"L\""..escapeSpecial(o)..L"\""
	elseif type(o) == "boolean" then
		if o then
			result = L"Bt"
		else
			result = L"Bf"
		end
	elseif type(o) == "table" then
		result = L"{"..towstring(level)..L"{"
		for k, v in pairs(o) do
			result = result..L"["..serialise(k, level + 1)..L"]="..serialise(v, level + 1)..L","
		end
		result = result..L"}"..towstring(level)..L"}"
	else
		error("cannot serialise a "..type(o))
	end

	return result
end

-- deserialise a wstring output by LibCereal.serialise, returning the table or value
deserialise = function(str, level)

	if type(str) ~= "wstring" then
		error("cannot deserialise a "..type(str))
	end

	local o

	-- boolean
	if str:match(L"^B") then
		-- match t/f or true/false for older versions
		if str == L"Bt" or str == L"Btrue" then
			o = true
		elseif str == L"Bf" or str == L"Bfalse" then
			o = false
		end

	-- compressed string
	elseif str:match(L"^S�") then
		if not stringCompressionTable then
			error("string compression table not specified")
		end
		o = stringCompressionTable[tostring(str):gsub("^S", "")] or ""

	-- string
	elseif str:match(L"^S") then
		o = tostring(unEscapeSpecial(str:gsub(L"^S\"", L""):gsub(L"\"$", L"")))

	-- wstring
	elseif str:match(L"^L") then
		o = unEscapeSpecial(str:gsub(L"^L\"", L""):gsub(L"\"$", L""))

	-- number
	elseif str:match(L"^N") then
		o = tonumber(tostring(str:gsub(L"^N", L"")))

	-- table
	elseif str:match(L"^{") then
		o = {}

		-- remove the table delimiters
		str = str:gsub(L"^{"..towstring(level)..L"{", L""):gsub(L"^}"..towstring(level)..L"}$", L"")

		-- only pull items from nonempty tables
		if str ~= "" then

			-- retrieve the first field
			local k, v
			k, str = str:match(L"^%[(.-[^\\])%]=(.*)")
			while k and str do

				-- retrieve the value - it could be a table
				if str:match(L"^{") then
					v, str = str:match(L"^({"..towstring(level + 1)..L"{.-}"..towstring(level + 1)..L"}),(.*)$")
				else
					v, str = str:match(L"(.-[^\\]),(.*)$")
				end
				if k and v then
					o[deserialise(k, level + 1)] = deserialise(v, level + 1)
				end

				-- retrieve the next field
				k, str = str:match(L"^%[(.-[^\\])%]=(.*)")
			end
		end
	end

	return o
end
